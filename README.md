# Tailwind CSS Colours

GIMP / Inkscape Colour Palette based on Official Tailwind CSS colours


It is a .gpl file which you can use with Inkscape and GIMP to get Tailwind CSS' offical colours as provided in the link below

Source : https://tailwindcss.com/docs/customizing-colors  


![Tailwind css](https://gitlab.com/meetdilip/tailwind-css-colours/-/raw/main/Tailwind_CSS_colours.png)
